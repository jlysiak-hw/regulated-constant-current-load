# Simple regulated constant current load 

## Schematic

![sch][sch]

## PCB

![pcb][pcb]

[pcb]:./imgs/pcb.png
[sch]:./imgs/schematic.png

